# AUA Tinder #


### What is this repository for? ###

* Quick summary
AUATinder is a REST api, based on a dataset of registered users of AUA Moodle.
Currently supports Like queries in SQL syntax as well as a full dataset preview in both JSON and XML.
* 0.2

### How do I get set up? ###

* Configuration
Clone the Repository on your local machine and open it in Visual Studio( preferably 2013 or 2015 with .NET Framework 4.5 or later).
AUATinder is based on an MSSQL SQL Express 2014 instance, running on Azure(which is Currently expired :( )

* Database configuration
You can create your own local instance with the provided dataset in .dat file and populate it using the provided DataCollector.


### API Call Examples ###

* http://auatinder.azurewebsites.net/api/EntryModels?search=Anna
* http://auatinder.azurewebsites.net/api/EntryModels?search=An%

### Who do I talk to? ###

* Karen Galstyan (5uperman) , Levon Stepanyan, Erik Arakelyan
* American University of Armenia