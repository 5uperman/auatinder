﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AUATinder.WebAPI.Models;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace AUATinder.WebAPI.Controllers
{
    public class EntryModelsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/EntryModels
        public IQueryable<EntryModel> GetEntryModels()
        {
            return db.EntryModels;
        }
        /*
        // GET: api/EntryModels/5
        [ResponseType(typeof(EntryModel))]
        public async Task<IHttpActionResult> GetEntryModel(int id)
        {
            EntryModel entryModel = await db.EntryModels.FindAsync(id);
            if (entryModel == null)
            {
                return NotFound();
            }

            return Ok(entryModel);
        }
        */

        // GET: api/EntryModels/5
        [ResponseType(typeof(EntryModel))]
        public IHttpActionResult GetEntryModelByString(string search)
        {

           var results= db.EntryModels
            .AsEnumerable()
            .Where(p => DataCollector.IsSqlLikeMatch(p.Name,search)).ToList();

            //.Where(oh => oh.Hierarchy.Contains("/12/"))
            /*
            search = search.Replace('%', '*');
            search = search.Replace('_', '?');
            var results = from c in db.EntryModels
                          where LikeOperator.LikeString(c.Name, search, CompareMethod.Text)
                          select c;
            */
            if (results == null)
            {
                return NotFound();
            }
           

            return Ok(results);
        }

        // PUT: api/EntryModels/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutEntryModel(int id, EntryModel entryModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != entryModel.Id)
            {
                return BadRequest();
            }

            db.Entry(entryModel).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EntryModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/EntryModels
        [ResponseType(typeof(EntryModel))]
        public async Task<IHttpActionResult> PostEntryModel(EntryModel entryModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EntryModels.Add(entryModel);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = entryModel.Id }, entryModel);
        }

        // DELETE: api/EntryModels/5
        [ResponseType(typeof(EntryModel))]
        public async Task<IHttpActionResult> DeleteEntryModel(int id)
        {
            EntryModel entryModel = await db.EntryModels.FindAsync(id);
            if (entryModel == null)
            {
                return NotFound();
            }

            db.EntryModels.Remove(entryModel);
            await db.SaveChangesAsync();

            return Ok(entryModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EntryModelExists(int id)
        {
            return db.EntryModels.Count(e => e.Id == id) > 0;
        }
    }
}