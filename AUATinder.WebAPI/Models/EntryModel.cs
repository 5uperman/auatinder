﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AUATinder.WebAPI.Models
{
    public class EntryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int gender { get; set; } //0 Female 1 Male 2 Unknown
        public string image { get; set;}
    }
}