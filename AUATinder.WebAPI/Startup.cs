﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using Swashbuckle;
[assembly: OwinStartup(typeof(AUATinder.WebAPI.Startup))]

namespace AUATinder.WebAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            //DataCollector.Collector();
        }
        
    }
}
