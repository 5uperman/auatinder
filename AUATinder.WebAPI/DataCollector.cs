﻿using AUATinder.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace AUATinder.WebAPI
{
    public static class DataCollector
    {
        public static void main()
        {
            Collector();
        }
        public static void Collector()
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    string[] lines = System.IO.File.ReadAllLines(@"C:\Users\scrival\Desktop\names.dat");
                    foreach (string line in lines)
                    {
                        if (!line.Equals("undef"))
                        {
                            string[] temp = line.Split(';');
                            EntryModel newEntry = new EntryModel()
                            {
                                Id = Int32.Parse(temp[0]),
                                Name = temp[1],
                                gender = Int32.Parse(temp[2]),
                                image = "http://auatinder.azurewebsites.net/images/" + temp[0] + ".JPEG"
                            };
                            db.EntryModels.Add(newEntry);
                        }
                    }
                    db.SaveChanges();
                }
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static bool Like(this string toSearch, string toFind)
        {
            return new Regex(@"\A" + new Regex(@"\.|\$|\^|\{|\[|\(|\||\)|\*|\+|\?|\\").Replace(toFind, ch => @"\" + ch).Replace('_', '.').Replace("%", ".*") + @"\z", RegexOptions.Singleline).IsMatch(toSearch);
        }
        public static bool IsSqlLikeMatch(string input, string pattern)
        {
            /* Turn "off" all regular expression related syntax in
            * the pattern string. */
            pattern = Regex.Escape(pattern);

            /* Replace the SQL LIKE wildcard metacharacters with the
            * equivalent regular expression metacharacters. */
            pattern = pattern.Replace("%", ".*?").Replace("_", ".");

            /* The previous call to Regex.Escape actually turned off
            * too many metacharacters, i.e. those which are recognized by
            * both the regular expression engine and the SQL LIKE
            * statement ([...] and [^...]). Those metacharacters have
            * to be manually unescaped here. */
            pattern = pattern.Replace(@"\[", "[").Replace(@"\]",
            "]").Replace(@"\^", "^");

            return Regex.IsMatch(input, pattern);
        }
    }
}